<?php


Route::filter('auth', function()
{	
	if (!Connect::isLoggedIn()) return Redirect::to('login');

});

Route::filter('admin-auth', function()
{	
		// Check if the user is logged in
	if(!Connect::isLoggedIn())
	{
		// Store the current uri in the session
		Session::put('loginRedirect', Request::url());

		// Redirect to the login page
		return Redirect::to('login');
	}

	// Check if the user has access to the admin page
	if (!Connect::hasAccess('admin'))
	{
		// Show the insufficient permissions page
		dd('Not admin');
	}
	
});
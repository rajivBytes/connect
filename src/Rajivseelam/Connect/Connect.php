<?php namespace Rajivseelam\Connect;

use Cartalyst\Sentry\Sentry;

class Connect {


	protected $sentry;

	/**
	 * Constructor for Connect Library
	 */
	
	public function __construct()
	{
		$this->sentry = \App::make('sentry');
	}

	/**
	 * Pass Credentails as in Username and Password of a user
	 * and authenticate.
	 * 
	 * This method can be improved to support oauth login,
	 * where a token is passed with header and we authenticate.
	 * 
	 * 
	 * @param  [type] $credentials [description]
	 * @return [type]              [description]
	 */
	public function authenticate($credentials)
	{
		return $this->sentry->authenticate($credentials,false);
	}

	/**
	 * Get current logged in user
	 * 
	 * The user model returned by this model is default sentry user model unless
	 * changed in sentry config files.
	 * 
	 * @return The User Model
	 */
	public function currentUser()
	{
		return $this->sentry->getUser();
	}

	/**
	 * Check if a user is logged in.
	 * 
	 * Here we used default check method of sentry,
	 * if we support token login, we have check if
	 * the token is valid and log him in let's say 
	 * using $this->sentry->login($user)
	 *
	 * @return void
	 * @author 
	 **/
	public function isLoggedIn()
	{
		return $this->sentry->check();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	public function hasAccess($permissions)
	{
		return $this->sentry->getUser()->hasAccess($permissions);
	}

	/**
	 * Logout a user
	 *
	 * @return void
	 * @author 
	 **/
	public function logout()
	{
		return $this->sentry->logout();
	}


}